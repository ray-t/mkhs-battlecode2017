package FirstPlayer;
import battlecode.common.*;

import java.util.Map;

/**
 *
 * Created by raytr on 1/20/2017.
 */
public class BotBase {
    public static RobotController rc;
    static MapLocation enemyArchons[];
    static MapLocation archons[];
    static MapLocation closestInitEnemyArchon;
    static MapLocation closestInitArchon;

    public static void init(RobotController theRC) throws GameActionException {
        rc = theRC;
        archons = rc.getInitialArchonLocations(rc.getTeam());
        closestInitArchon = archons[0];
        for (MapLocation archon : archons){
            if (rc.getLocation().distanceTo(archon) < rc.getLocation().distanceTo(closestInitArchon)) closestInitArchon = archon;
        }


        enemyArchons = rc.getInitialArchonLocations(rc.getTeam().opponent());
        closestInitEnemyArchon = enemyArchons[0];
        for (MapLocation archon : enemyArchons){
            if (rc.getLocation().distanceTo(archon) < rc.getLocation().distanceTo(closestInitEnemyArchon)) closestInitEnemyArchon = archon;
        }

    }


    /**
     * Attempts to move in a given direction, while avoiding small obstacles directly in the path.
     *
     * @param dir The intended direction of movement
     * @return true if a move was performed
     * @throws GameActionException
     */
    static boolean tryMove(Direction dir) throws GameActionException {
        return tryMove(dir,20,3);
    }

    /**
     * Attempts to move in a given direction, while avoiding small obstacles direction in the path.
     *
     * @param dir The intended direction of movement
     * @param degreeOffset Spacing between checked directions (degrees)
     * @param checksPerSide Number of extra directions checked on each side, if intended direction was unavailable
     * @return true if a move was performed
     * @throws GameActionException
     */
    static boolean tryMove(Direction dir, float degreeOffset, int checksPerSide) throws GameActionException {
        if (rc.hasMoved()) return false;

        // First, try intended direction
        if (rc.canMove(dir)) {
            rc.move(dir);
            return true;
        }

        // Now try a bunch of similar angles
        boolean moved = false;
        int currentCheck = 1;

        while(currentCheck<=checksPerSide) {
            // Try the offset of the left side
            if(rc.canMove(dir.rotateLeftDegrees(degreeOffset*currentCheck))) {
                rc.move(dir.rotateLeftDegrees(degreeOffset*currentCheck));
                return true;
            }
            // Try the offset on the right side
            if(rc.canMove(dir.rotateRightDegrees(degreeOffset*currentCheck))) {
                rc.move(dir.rotateRightDegrees(degreeOffset*currentCheck));
                return true;
            }
            // No move performed, try slightly further
            currentCheck++;
        }

        // A move never happened, so return false.
        return false;
    }

    /**
     * Donates all the bullets for victory points if possible
     * The price of a victory point increases linearly over the course of the game,
     * starting at 7.5 bullets per victory point on round 1, and increasing to 20 bullets by round 3000.
     *
     */
    static void checkPay2Win() throws GameActionException{
        if (rc.getTeamBullets() - 5 > (rc.getVictoryPointCost() * 1000)) rc.donate(rc.getTeamBullets());
    }

    /**
     * Shaking trees is free - just do it!
     */
    public static void shakeNearbyTrees() throws GameActionException{
        TreeInfo closeTrees[] = rc.senseNearbyTrees(-1,Team.NEUTRAL);
        if (closeTrees.length != 0){
            if (rc.canShake(closeTrees[0].getLocation())) rc.shake(closeTrees[0].getLocation());
        }

    }

    /**
     * Pathfinding attempt
     *
     * Bug
     * @return true if we're close enough, false otherwise
     */
    private static boolean isBugging;
    private static float distBeforeBugging;
    private static Direction buggingDir;


   public static boolean bugPathTo(MapLocation goalLoc) throws GameActionException {
       int tryCounter = 0;
       float distToGoal = rc.getLocation().distanceTo(goalLoc);
       Direction goalDir = rc.getLocation().directionTo(goalLoc);

       if (distToGoal < rc.getType().bodyRadius * 2) return true;
       //Don't waste the turn; make a move!
       while (!rc.hasMoved()) {
           if (!isBugging) {
               if (rc.canMove(goalDir)){
                   rc.move(goalDir);
               }else { //If we can't move, then we should start bugging
                   distBeforeBugging = distToGoal;
                   //initialize buggindir
                   buggingDir = goalDir;
                   isBugging = true;
                   //System.out.println("I AM BUGGING");
               }
           } else {
               //If we're closer now than we were when we started + we can move in the ideal dir, leave bug
               //+ if after moving then we will still be closer
               if (distToGoal < distBeforeBugging && rc.canMove(goalDir)) {
                   isBugging = false;
                   rc.move(goalDir);
                   //System.out.println("NO LONGER BUGGING");
               } else { //Otherwise, keep bugging
                   int checkCount = 1;
                   int degOffsetPerCheck = 5;
                   //Go left by default

                   buggingDir = buggingDir.rotateLeftDegrees(30);
                   while (!rc.canMove(buggingDir)) {
                       //Following an obstacle is rotating then rotating back
                       buggingDir = buggingDir.rotateRightDegrees(degOffsetPerCheck);
                       checkCount++;
                   }
                   rc.move(buggingDir);
               }
           }
           tryCounter++;
           if (tryCounter > 7) break; //Give up
       }
       return false;
   }


   public static void tryDodgeNearbyBullets() throws GameActionException{
       if (rc.senseNearbyBullets().length !=0) { //only run this expensive method if there are bullets near anyway
           //dodge bullets
           MapLocation dodgeLoc = getBestBulletDodgeLoc();
           if (dodgeLoc != null && !rc.hasMoved()){
               rc.move(dodgeLoc);
               System.out.println("Attempt bullet dodge.");
           }
       }
   }
    /**
     * Get possible directions - 6
     * Calculate the number of collisions we would have if we were to move to this pos
     * Return the best position to move to to
     * TODO test if this even works
     * Already checks if spot is valid
     * @return null if all the locations have equal bullet collisions; otherwise the MapLocation with the fewest bullet collisions
     */
   public static MapLocation getBestBulletDodgeLoc(){
       BulletInfo nearbyBullets[] = rc.senseNearbyBullets();
       if (nearbyBullets.length == 0) return null; //No bullets, equal # of collisions :^)

       float botStrideRadius = rc.getType().strideRadius;
       MapLocation botLoc = rc.getLocation();
       MapLocation possibleLoc[] = {
               botLoc.add(Direction.NORTH, botStrideRadius),
               botLoc.add(Direction.NORTH.rotateRightDegrees(60), botStrideRadius),
               botLoc.add(Direction.NORTH.rotateRightDegrees(120), botStrideRadius),
               botLoc.add(Direction.NORTH.rotateRightDegrees(180), botStrideRadius),
               botLoc.add(Direction.NORTH.rotateRightDegrees(240), botStrideRadius),
               botLoc.add(Direction.NORTH.rotateRightDegrees(300), botStrideRadius)
       };

       boolean hasBestLoc = false; //Used to check if there is a best loc anyway
       MapLocation fewestBulletsLoc = possibleLoc[0]; //Start with first
       for (MapLocation loc : possibleLoc){
           if (getNumBulletCollisions(loc,nearbyBullets) < getNumBulletCollisions(fewestBulletsLoc,nearbyBullets)
                   && rc.canMove(loc)){ //make sure its a valid space
               fewestBulletsLoc = loc;
               hasBestLoc = true;
           }
       }

       return !hasBestLoc ? null : fewestBulletsLoc;
   }

   public static int getNumBulletCollisions(MapLocation desiredLoc,BulletInfo nearbyBullets[]){
       int bulletCollisions = 0;
       for (BulletInfo bulletInfo : nearbyBullets){
           if (willCollideWithMe(desiredLoc,bulletInfo)) bulletCollisions++;
       }
       return bulletCollisions;
   }



    /**
     * A slightly more complicated example function, this returns true if the given bullet is on a collision
     * course with the current robot. Doesn't take into account objects between the bullet and this robot.
     *
     * @param bullet The bullet in question
     * @return True if the line of the bullet's path intersects with this robot's current position.
     */
    static boolean willCollideWithMe(MapLocation myLocation,BulletInfo bullet) {
        // Get relevant bullet information
        Direction propagationDirection = bullet.dir;
        MapLocation bulletLocation = bullet.location;

        // Calculate bullet relations to this robot
        Direction directionToRobot = bulletLocation.directionTo(myLocation);
        float distToRobot = bulletLocation.distanceTo(myLocation);
        float theta = propagationDirection.radiansBetween(directionToRobot);

        // If theta > 90 degrees, then the bullet is traveling away from us and we can break early
        if (Math.abs(theta) > Math.PI/2) {
            return false;
        }

        // distToRobot is our hypotenuse, theta is our angle, and we want to know this length of the opposite leg.
        // This is the distance of a line that goes from myLocation and intersects perpendicularly with propagationDirection.
        // This corresponds to the smallest radius circle centered at our location that would intersect with the
        // line that is the path of the bullet.
        float perpendicularDist = (float)Math.abs(distToRobot * Math.sin(theta)); // soh cah toa :)

        return (perpendicularDist <= rc.getType().bodyRadius);
    }

}
