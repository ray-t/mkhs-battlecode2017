package FirstPlayer;
import battlecode.common.*;

import java.util.HashSet;


/**
 * Created by raytr on 1/20/2017.
 */
public class BotLumberjack extends BotBase{
    public static void loop() throws GameActionException {
        boolean moveToArchons = true;
        HashSet<MapLocation> radioEnemyLocs = new HashSet<>(); //We want this to be a HashSet because duplicate keys mean nothing

        while(true){
            checkPay2Win();
            shakeNearbyTrees();
            try {
                RobotInfo[] enemies = rc.senseNearbyRobots(-1,rc.getTeam().opponent());

                if (BroadcastHandler.canClearBroadcastYet()){
                    if (!BroadcastHandler.getClearedToBroadcast()) {
                        BroadcastHandler.clearAllBroadcasts();
                        BroadcastHandler.setClearedToBroadcast(true);
                    }
                }
                if (BroadcastHandler.canSendBroadcastYet()) {
                    BroadcastHandler.sendBroadcast(BroadcastHandler.BroadcastType.HEADCOUNT,0,0,0);
                    if (enemies.length !=0){
                        BroadcastHandler.sendBroadcast(BroadcastHandler.BroadcastType.ENEMY_SPOTTED,
                                BroadcastHandler.getEnemySpottedTypeFlag(enemies[0].getType()),
                                (int) enemies[0].getLocation().x,
                                (int) enemies[0].getLocation().y);
                    }
                }
                if (BroadcastHandler.canReceiveBroadcastYet()) {
                    BroadcastHandler.setClearedToBroadcast(false);
                    radioEnemyLocs.clear();
                    HashSet<Integer> enemyBroadcasts = BroadcastHandler.receiveBroadcasts(BroadcastHandler.BroadcastType.ENEMY_SPOTTED);
                    for (Integer broadcast : enemyBroadcasts){
                        int[] parsed = BroadcastHandler.parseBroadcastValue(broadcast);
                        radioEnemyLocs.add(new MapLocation(parsed[1],parsed[2]));
                    }
                }

                tryDodgeNearbyBullets();
                if (enemies.length != 0){
                    MapLocation enemyLoc = enemies[0].getLocation();
                    MapLocation ourLoc = rc.getLocation();
                    //senseNearbyRobots returns in order of increasing dist
                    tryMove(ourLoc.directionTo(enemyLoc));
                    if (ourLoc.distanceTo(enemyLoc) < RobotType.LUMBERJACK.bodyRadius + GameConstants.LUMBERJACK_STRIKE_RADIUS){
                        System.out.println("STRIKE?");
                        if (rc.canStrike()){
                            System.out.println("STRIKE!");
                            rc.strike();
                        }
                    }

                }else{
                    if (rc.getLocation().distanceTo(closestInitEnemyArchon) < 10) moveToArchons = false;
                    if (!moveToArchons && radioEnemyLocs.size() != 0 ){
                        System.out.println("Radio enemies!!!");
                        MapLocation closest = new MapLocation(900,900); //Initialize to some super far loc
                        for (MapLocation enemy:radioEnemyLocs){
                            if (rc.getLocation().distanceTo(enemy) < rc.getLocation().distanceTo(closest)) closest = enemy;
                        }
                        System.out.println("Closest radioEnemy" + closest.x + closest.y);
                        bugPathTo(closest);
                    }else{
                        //Move towards the enemy archons
                       tryMove(rc.getLocation().directionTo(closestInitEnemyArchon));

                    }
                }

                //No trees allowed ever
                destroyNearbyTrees();

                Clock.yield();
            } catch (Exception e) {
                System.out.println("Lumberjack Exception");
                e.printStackTrace();
            }
        }
    }

    /**
     * sense and destroy nearby trees
     * @throws GameActionException
     */
    public static void destroyNearbyTrees() throws GameActionException{
        //Get all nearby trees
        //You can only chop down trees within interaction range
        TreeInfo closeTrees[] = rc.senseNearbyTrees(RobotType.LUMBERJACK.bodyRadius + GameConstants.INTERACTION_DIST_FROM_EDGE);
        if (closeTrees.length == 0) return;

        //senseNearbyTrees() returns in order of increasing distance
        MapLocation closestTreeLoc = closeTrees[0].getLocation();
        if (rc.canChop(closestTreeLoc)){
            rc.chop(closestTreeLoc);
        }


    }
}
