package FirstPlayer;

import battlecode.common.*;

import java.awt.*;
import java.util.HashSet;

/**
 * Created by raytr on 1/22/2017.
 * USE CHANNEL 9999 as a boolean to see if the channels were cleared yet
 * Let the Archons clear
 * LOCATION
 *      ARCHON     0-249
 *      GARDENER   250-499
 *      SOLDIER    500-749
 *      TANK       750-999
 *      SCOUT      1000-1249
 *      LUMBERJACK 1250-1499
 * ENEMY_SPOTTED
 *      1500-1999
 * HEADCOUNT
 *      ARCHON     9990
 *      GARDENER   9991
 *      SOLDIER    9992
 *      TANK       9993
 *      SCOUT      9994
 *      LUMBERJACK 9995
 *
 * [flag][x-coordinate][y-coordinate]
 *
 */
public class BroadcastHandler {
    static RobotController rc;
    public static void init(RobotController theRc) {
        rc = theRc;
    }


    //the channels used for each BroadcastType, INCLUSIVE
    private static int CHANNEL_LOCATION_START = 0;
    private static int CHANNEL_LOCATION_END = 1499;
    private static int CHANNEL_ENEMY_SPOTTED_START = 1500;
    private static int CHANNEL_ENEMY_SPOTTED_END = 1999;
    private static int CHANNEL_HEADCOUNT_START = 9990;
    private static int CHANNEL_HEADCOUNT_END = 9995;
    //the # of channels per robot type
    private static int CHANNELS_PER_ROBOTTYPE = 250;

    //used as a simple boolean channel to see if some bot already cleared the channels
    private static int CHANNEL_BOOLEAN_CLEAR_TO_BROADCAST = 9999;


    enum BroadcastType{
        LOCATION,HEADCOUNT,ENEMY_SPOTTED
    }


    public static boolean canClearBroadcastYet(){
        return rc.getRoundNum() % 10 == 9;
    }
    public static boolean canSendBroadcastYet(){
        return rc.getRoundNum() % 10 == 0;
    }
    public static boolean canReceiveBroadcastYet(){
        return rc.getRoundNum() % 10 == 1;
    }


    public static boolean getClearedToBroadcast() throws GameActionException{
        return rc.readBroadcastBoolean(CHANNEL_BOOLEAN_CLEAR_TO_BROADCAST);
    }
    public static void setClearedToBroadcast(boolean cleared) throws GameActionException{
        rc.broadcast(CHANNEL_BOOLEAN_CLEAR_TO_BROADCAST,(cleared ? 1 : 0));
    }


    public static void clearAllBroadcasts() throws GameActionException,Exception{
        if (!canClearBroadcastYet()) throw new Exception("CUSTOM Tried clearing broadcast before ready");
        //Clear LOCATION broadcasts
        for (int i=CHANNEL_LOCATION_START;i<=CHANNEL_LOCATION_END;i++){
            if (!rc.readBroadcastBoolean(i)) break;
            else rc.broadcast(i,0);
        }
        //Clear HEADCOUNT broadcasts
        for (int i=CHANNEL_HEADCOUNT_START;i<=CHANNEL_HEADCOUNT_END;i++){
            rc.broadcast(i,0);
        }
        //Clear ENEMY_SPOTTED broadcasts
        for (int i=CHANNEL_ENEMY_SPOTTED_START;i<=CHANNEL_ENEMY_SPOTTED_END;i++){
            if (!rc.readBroadcastBoolean(i)) break;
            else rc.broadcast(i,0);
        }

    }
    public static void sendBroadcast(BroadcastType type, int flag, int xCoord, int yCoord) throws GameActionException, Exception{
        if (!canSendBroadcastYet()) throw new Exception("CUSTOM Tried sending broadcast before ready.");
        int robotTypeIndex = getRobotTypeIndex(rc.getType());
        int loopStartIndex = 0;
        int loopEndIndex = 0;
        switch (type){
            case HEADCOUNT:
                rc.broadcast(CHANNEL_HEADCOUNT_START + robotTypeIndex,1 + rc.readBroadcast(CHANNEL_HEADCOUNT_START + robotTypeIndex));
                return; //Just end here; skip loop
            case LOCATION:
                loopStartIndex = (robotTypeIndex * CHANNELS_PER_ROBOTTYPE) -1;
                loopEndIndex = ((robotTypeIndex* CHANNELS_PER_ROBOTTYPE) + CHANNELS_PER_ROBOTTYPE) -1;
                break;
            case ENEMY_SPOTTED:
                loopStartIndex = CHANNEL_ENEMY_SPOTTED_START;
                loopEndIndex = CHANNEL_ENEMY_SPOTTED_END;
                break;
        }
        int desiredBroadcast = Integer.parseInt(""+flag+""+getPaddedCoordString(xCoord)+""+getPaddedCoordString(yCoord));
        for (int i=loopStartIndex;i<=loopEndIndex;i++){
            if (!rc.readBroadcastBoolean(i)){ //Check for free space
                rc.broadcast(i,desiredBroadcast); //write to free space & return
                return;
            }
        }
    }


    public static int getHeadCount(RobotType rt) throws GameActionException{
        return rc.readBroadcastInt(CHANNEL_HEADCOUNT_START + getRobotTypeIndex(rt));
    }

    public static HashSet<Integer> receiveBroadcasts(BroadcastType broadcastType) throws GameActionException, Exception{
        return receiveBroadcasts(broadcastType,rc.getType());
    }
    public static HashSet<Integer> receiveBroadcasts(BroadcastType broadcastType, RobotType RobotType) throws GameActionException, Exception{
        if (!canReceiveBroadcastYet()) throw new Exception("CUSTOM Tried receiving broadcast before ready.");
        HashSet<Integer> broadcasts = new HashSet<>();

        int robotTypeIndex = getRobotTypeIndex(RobotType);
        int loopStartIndex = 0;
        int loopEndIndex = 0;
        switch (broadcastType){
            case HEADCOUNT:
                throw new IllegalArgumentException("tried to use HEADCOUNT in receiveBroadcasts(),  which returns a hashTable. Use getHeadcount() instead");
            case LOCATION:
                loopStartIndex = (robotTypeIndex * CHANNELS_PER_ROBOTTYPE) -1;
                loopEndIndex = ((robotTypeIndex* CHANNELS_PER_ROBOTTYPE) + CHANNELS_PER_ROBOTTYPE) -1;
                break;
            case ENEMY_SPOTTED:
                loopStartIndex = CHANNEL_ENEMY_SPOTTED_START;
                loopEndIndex = CHANNEL_ENEMY_SPOTTED_END;
                break;
        }
        for (int i=loopStartIndex;i<=loopEndIndex;i++){
            if (!rc.readBroadcastBoolean(i)) break;
            else broadcasts.add(rc.readBroadcastInt(i));

        }

        return broadcasts;
    }



    /**
     * Takes a recieved broadcast value and returns an array that splits it into its components
     * @param value raw integer return value
     * @return int array - arr[0]: code; arr[1]:timestamp; arr[2]:extra flag; arr[3]coords x arr[4] coords y
     */
    public static int[] parseBroadcastValue(int value) throws IllegalArgumentException {
        if (Util.getNumberDigits(value) != 7) throw new IllegalArgumentException("Parsed broadcast value must have 7 digits");
        //out.println(value);
        String valuesString = "" + value;
        int[] values = {0,0,0};
        for (int i=0;i<3;i++){
            if (i == 0) values[i] = Integer.parseInt(valuesString.substring(0,1));
            if (i == 1) values[i] = Integer.parseInt(valuesString.substring(1,4));
            if (i == 2) values[i] = Integer.parseInt(valuesString.substring(4));
        }
        System.out.println(values[0] + " " + values[1] + " " + values[2]);
        return values;

    }

    public static int getEnemySpottedTypeFlag(RobotType rt){
        return getRobotTypeIndex(rt) + 1;
    }
    public static RobotType getRobotTypeFromFlag(int flag){
        RobotType type = RobotType.ARCHON;
        switch (flag){
            case 1:
                type = RobotType.ARCHON;
                break;
            case 2:
                type = RobotType.GARDENER;
                break;
            case 3:
                type = RobotType.SOLDIER;
                break;
            case 4:
                type = RobotType.TANK;
                break;
            case 5:
                type = RobotType.SCOUT;
                break;
            case 6:
                type = RobotType.LUMBERJACK;
                break;
        }
        return type;
    }

    private static int getRobotTypeIndex(RobotType rt){
        int typeIndex = 0;
        switch (rt) {
            case ARCHON:
                typeIndex = 0;
                break;
            case GARDENER:
                typeIndex = 1;
                break;
            case SOLDIER:
                typeIndex = 2;
                break;
            case TANK:
                typeIndex = 3;
                break;
            case SCOUT:
                typeIndex = 4;
                break;
            case LUMBERJACK:
                typeIndex = 5;
                break;
        }
        return typeIndex;
    }

    /**
     * Turns an INT coordinate into a string, adding 0s to the front to pad if required
     * @param coordinate
     * @return coordinate as string <= 999
     */
    private static String getPaddedCoordString(int coordinate) {
        return ("000" + coordinate).substring(String.valueOf(coordinate).length());
    }


}
