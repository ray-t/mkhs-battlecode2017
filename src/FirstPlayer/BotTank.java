package FirstPlayer;
import battlecode.common.*;
/**
 * Created by raytr on 1/29/2017.
 */
public class BotTank extends BotBase{
    public static void loop(){
        while(true){
            try{
                RobotInfo enemies[] = rc.senseNearbyRobots(-1,rc.getTeam().opponent());
                checkPay2Win();
                shakeNearbyTrees();
                tryMove(rc.getLocation().directionTo(closestInitEnemyArchon));
                if (enemies.length != 0){
                    if (rc.canFireSingleShot()){
                        rc.fireSingleShot(rc.getLocation().directionTo(enemies[0].getLocation()));
                    }
                }

            }catch (Exception e){
                System.out.println("Tank Exception");
                e.printStackTrace();
            }
        }
    }

}
