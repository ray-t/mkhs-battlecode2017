package FirstPlayer;

import battlecode.common.*;
/**
 * Created by raytr on 1/20/2017.
 */
public class RobotPlayer {
    public static void run(RobotController rc) throws GameActionException {
        BroadcastHandler.init(rc);
        BotBase.init(rc);
        switch (rc.getType()) {
            case ARCHON:
                BotArchon.loop();
                break;
            case GARDENER:
                BotGardener.loop();
                break;
            case SOLDIER:
                BotSoldier.loop();
                break;
            case TANK:
                BotTank.loop();
                break;
            case LUMBERJACK:
                BotLumberjack.loop();
                break;
            case SCOUT:
                BotScout.loop();
                break;
            default:
                throw new IllegalArgumentException("some sort of alien robot type");

        }
    }
}
