package FirstPlayer;
import battlecode.common.*;
/**
 * Created by raytr on 1/22/2017.
 */
public class BotScout extends BotBase {
    public static void loop() throws GameActionException{
        Direction randDir = Util.getRandomDir();
        Direction dirToAdd = Direction.NORTH;
        MapLocation goalLoc = new MapLocation(0,0);
        while (true){
            try{
                checkPay2Win();
                shakeNearbyTrees();



                RobotInfo enemies[] = rc.senseNearbyRobots(-1,rc.getTeam().opponent());
                if (BroadcastHandler.canClearBroadcastYet()){
                    if (!BroadcastHandler.getClearedToBroadcast()) {
                        BroadcastHandler.clearAllBroadcasts();
                        BroadcastHandler.setClearedToBroadcast(true);
                    }
                }
                if (BroadcastHandler.canSendBroadcastYet()) {
                    BroadcastHandler.sendBroadcast(BroadcastHandler.BroadcastType.HEADCOUNT,0,0,0);
                    if (enemies.length !=0){
                        BroadcastHandler.sendBroadcast(BroadcastHandler.BroadcastType.ENEMY_SPOTTED,
                                BroadcastHandler.getEnemySpottedTypeFlag(enemies[0].getType()),
                                (int) enemies[0].getLocation().x,
                                (int) enemies[0].getLocation().y);
                    }
                }


                tryDodgeNearbyBullets();
                if (enemies.length == 0){
                    tryMove(rc.getLocation().directionTo(closestInitEnemyArchon));
                }else {
                    RobotInfo mainEnemy = enemies[0];
                    for (RobotInfo enemy : enemies){
                        if (enemy.type == RobotType.GARDENER){
                            mainEnemy = enemy;
                            break;
                        }
                    }
                    if (rc.canFireSingleShot()) rc.fireSingleShot(rc.getLocation().directionTo(mainEnemy.getLocation()));

                    goalLoc = mainEnemy.getLocation().add(dirToAdd);
                    tryMove(rc.getLocation().directionTo(goalLoc));
                    if (rc.getLocation().distanceTo(goalLoc) < 2) dirToAdd = dirToAdd.rotateRightRads((float) Math.PI/3);


                }


                Clock.yield();
            }catch (Exception e) {
                System.out.println("Scout exception");
                e.printStackTrace();
            }
        }
    }
}
