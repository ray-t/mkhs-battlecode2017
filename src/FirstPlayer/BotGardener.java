package FirstPlayer;

import battlecode.common.*;

import java.util.HashSet;


/**
 * Created by raytr on 1/20/2017.
 */
public class BotGardener extends BotBase {
    public static void loop() throws GameActionException {
        Direction randomDir = Util.getRandomDir();
        MapLocation leaveArchonLoc = closestInitArchon.add(Util.getRandomDir(),10); //Initalize to whatever
        HashSet<MapLocation> radioGardenerLocs = new HashSet<>();
        int roundsAlive = 0;

        Direction spawnDir = rc.getLocation().directionTo(closestInitEnemyArchon);
        
        boolean scannedForGardeners = false, haveNearbyGardeners = false;
        MapLocation plantTrees = null;
        while (true) {
            try {

                checkPay2Win();
                shakeNearbyTrees();
                RobotInfo enemies[] = rc.senseNearbyRobots(-1,rc.getTeam().opponent());
                if (BroadcastHandler.canClearBroadcastYet()){
                    if (!BroadcastHandler.getClearedToBroadcast()) {
                        BroadcastHandler.clearAllBroadcasts();
                        BroadcastHandler.setClearedToBroadcast(true);
                    }
                }
                if (BroadcastHandler.canSendBroadcastYet()){
                    BroadcastHandler.sendBroadcast(BroadcastHandler.BroadcastType.LOCATION,0,(int)rc.getLocation().x,(int)rc.getLocation().y);
                    BroadcastHandler.sendBroadcast(BroadcastHandler.BroadcastType.HEADCOUNT,0,0,0);
                    if (enemies.length !=0){
                        BroadcastHandler.sendBroadcast(BroadcastHandler.BroadcastType.ENEMY_SPOTTED,
                                BroadcastHandler.getEnemySpottedTypeFlag(enemies[0].getType()),
                                (int) enemies[0].getLocation().x,
                                (int) enemies[0].getLocation().y);
                    }
                }
                if (BroadcastHandler.canReceiveBroadcastYet()){
                    BroadcastHandler.setClearedToBroadcast(false);
                    radioGardenerLocs.clear();
                    HashSet<Integer> gardenerBroadcasts = BroadcastHandler.receiveBroadcasts(BroadcastHandler.BroadcastType.ENEMY_SPOTTED);
                    for (Integer broadcast : gardenerBroadcasts){
                        int[] parsed = BroadcastHandler.parseBroadcastValue(broadcast);
                        radioGardenerLocs.add(new MapLocation(parsed[1],parsed[2]));
                    }
                    //System.out.println("GARDENER - THERE ARE " + BroadcastHandler.getHeadCount(RobotType.GARDENER) + "Gardeners");
                }


                //Move away from init archon until we can plant a tree flowers
                if (closestInitArchon.distanceTo(rc.getLocation()) < 5 && roundsAlive < 20) {
                    Direction randDir = Util.getRandomDir();
                    bugPathTo(leaveArchonLoc);
                    TreeInfo nearbyNeutralTrees[] = rc.senseNearbyTrees(-1, Team.NEUTRAL);
                    if (nearbyNeutralTrees.length > 4 || enemies.length !=0) {
                        if (rc.canBuildRobot(RobotType.LUMBERJACK,randDir)) {
                            rc.buildRobot(RobotType.LUMBERJACK, randDir);
                        }
                    }else{
                        if (rc.canBuildRobot(RobotType.SCOUT, randDir)) {
                            rc.buildRobot(RobotType.SCOUT, randDir);
                        }
                    }




                } else {
                	if(!scannedForGardeners){
                		RobotInfo[] allies = rc.senseNearbyRobots(-1, rc.getTeam());
                		
                		for(RobotInfo ally:allies){
                			if(ally.getType() == RobotType.GARDENER){
                				plantTrees = ally.getLocation().add(rc.getLocation().directionTo(ally.getLocation()).opposite(), 9);
                				haveNearbyGardeners = true;
                				break;
                			}
                		}
                		scannedForGardeners = true;
                	}
                	if(haveNearbyGardeners){
                		boolean nearbyGardeners = false;
                		RobotInfo[] allies = rc.senseNearbyRobots(-1, rc.getTeam());
                		bugPathTo(plantTrees);
                		for(RobotInfo ally : allies){
                			if(ally.getType() == RobotType.GARDENER){
                				nearbyGardeners = true;
                				break;
                			}
                				
                		}
                		if(!nearbyGardeners){
                			haveNearbyGardeners = false;
                		}else if(rc.getLocation().isWithinDistance(plantTrees, 2)){
                			scannedForGardeners = false;
                		}
                	}else if (!plantTreeFlowerSpawnSpace(spawnDir) || enemies.length !=0) {

                        switch (Util.getRandomIntInclusive(0, 1)) {
                            case 0:
                                //Spawn more shit?
                                if (rc.canBuildRobot(RobotType.SOLDIER, spawnDir)) {
                                    rc.buildRobot(RobotType.SOLDIER, spawnDir);
                                }
                            case 1:
                                //Spawn shit
                                if (rc.canBuildRobot(RobotType.LUMBERJACK, spawnDir)) {
                                    rc.buildRobot(RobotType.LUMBERJACK, spawnDir);
                                }
                        }
                    }else{
                    	scannedForGardeners = true;
                    }
                }

                //Always water nearby trees
                waterNearbyTrees();

                roundsAlive++;
                Clock.yield();
            } catch (Exception e) {
                System.out.println("Gardener Exception");
                e.printStackTrace();
            }
        }
    }



    /**
     * Attempts to plants trees in a flower formation but leaves the top space empty to spawn troops in the north
     *
     * @return true if a tree was planted; false if no more trees can be planted
     */
    private static Boolean plantTreeFlowerSpawnSpace(Direction spawnDir) throws GameActionException {
        Direction plantDir = spawnDir;
        for (int i = 0; i < 6; i++) {
            //System.out.println("Checking plant");
            if (rc.canPlantTree(plantDir) && !plantDir.equals(spawnDir)) {
                rc.plantTree(plantDir);
                return true;
            } else {
                //   System.out.println("rotating plant dir");
                plantDir = plantDir.rotateRightRads((float) Math.PI / 3);
            }
        }
        return false;

    }

    /**
     * Waters the friendly tree with the lowest HP (within interaction radius)
     *
     * @throws GameActionException
     */
    private static void waterNearbyTrees() throws GameActionException {
        //Sense within interaction radius
        //Our team only
        TreeInfo closeTrees[] = rc.senseNearbyTrees(RobotType.GARDENER.bodyRadius + GameConstants.INTERACTION_DIST_FROM_EDGE, rc.getTeam());
        //leave if there are no close trees
        if (closeTrees.length == 0) return;


        //Initialize to 1st in array
        TreeInfo leastHealthyTree = closeTrees[0];
        for (TreeInfo nearbyTree : closeTrees) {
            if (nearbyTree.getHealth() < leastHealthyTree.getHealth()) leastHealthyTree = nearbyTree;
        }

        MapLocation leastHealthyTreeLoc = leastHealthyTree.getLocation();
        if (rc.canWater(leastHealthyTreeLoc)) {

            rc.water(leastHealthyTreeLoc);
        }

    }
}
