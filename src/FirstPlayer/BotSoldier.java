package FirstPlayer;
import battlecode.common.*;

import java.util.HashSet;

/**
 * Created by raytr on 1/20/2017.
 */
public class BotSoldier extends BotBase{
    public static void loop() throws GameActionException {
        boolean shouldShootSingleShot = true;
        boolean shouldShootPentaShot = false;
        HashSet<MapLocation> radioEnemyLocs = new HashSet<>(); //We want this to be a HashSet because duplicate keys mean nothing
        while(true){
            checkPay2Win();
            shakeNearbyTrees();
            try {
                RobotInfo[] enemies = rc.senseNearbyRobots(-1, rc.getTeam().opponent());
                if (enemies.length > 1){
                    shouldShootPentaShot = true;
                    shouldShootSingleShot = false;
                }else{
                    shouldShootPentaShot = false;
                    shouldShootSingleShot = true;
                }



                if (BroadcastHandler.canClearBroadcastYet()){
                    if (!BroadcastHandler.getClearedToBroadcast()) {
                        BroadcastHandler.clearAllBroadcasts();
                        BroadcastHandler.setClearedToBroadcast(true);
                    }
                }
                if (BroadcastHandler.canSendBroadcastYet()) {
                    BroadcastHandler.sendBroadcast(BroadcastHandler.BroadcastType.HEADCOUNT,0,0,0);
                    if (enemies.length !=0){
                        BroadcastHandler.sendBroadcast(BroadcastHandler.BroadcastType.ENEMY_SPOTTED,
                                BroadcastHandler.getEnemySpottedTypeFlag(enemies[0].getType()),
                                (int) enemies[0].getLocation().x,
                                (int) enemies[0].getLocation().y);
                    }
                }

                if (BroadcastHandler.canReceiveBroadcastYet()) {
                    BroadcastHandler.setClearedToBroadcast(false);
                    radioEnemyLocs.clear();
                    HashSet<Integer> enemyBroadcasts = BroadcastHandler.receiveBroadcasts(BroadcastHandler.BroadcastType.ENEMY_SPOTTED);
                    for (Integer broadcast : enemyBroadcasts){
                        int[] parsed = BroadcastHandler.parseBroadcastValue(broadcast);
                        radioEnemyLocs.add(new MapLocation(parsed[1],parsed[2]));
                    }
                }

                //Clear radioEnemies every 20 rounds DURING the broadcast BEFORE the recieveBroadcast
                //In case an enemy dies
                //if (rc.getRoundNum() % 20 == 0) radioEnemies.clear();


                /* ALWAYS SHOOT THINGS */
                if (enemies.length != 0) {
                    MapLocation enemyLoc = enemies[0].getLocation();
                    MapLocation ourLoc = rc.getLocation();
                    //Shoot bad guys
                    if (rc.canFirePentadShot() && shouldShootPentaShot) {
                        rc.firePentadShot(ourLoc.directionTo(enemyLoc));
                    } else if (rc.canFireSingleShot() && shouldShootSingleShot) {
                        rc.fireSingleShot(ourLoc.directionTo(enemyLoc));
                    }
                }

                /*ONLY MOVE IF WE DON'T SEE ENEMIES OR IF WE CAN DODGE BULLETS */
                tryDodgeNearbyBullets();
                if (enemies.length == 0){ //Only move if we see dont see enemies
                    if (radioEnemyLocs.size() != 0){
                        System.out.println("Radio enemies!!!");
                        MapLocation closest = new MapLocation(900,900); //Initialize to some super far loc
                        for (MapLocation enemy:radioEnemyLocs){
                            if (rc.getLocation().distanceTo(enemy) < rc.getLocation().distanceTo(closest)) closest = enemy;
                        }
                        System.out.println("Closest radioEnemy" + closest.x + closest.y);
                        bugPathTo(closest);
                    }else{
                        bugPathTo(closestInitEnemyArchon);
                    }
                }


                Clock.yield();
            } catch (Exception e) {
                System.out.println("Soldier Exception");
                e.printStackTrace();
            }
        }
    }
}
