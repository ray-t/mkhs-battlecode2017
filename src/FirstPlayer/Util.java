package FirstPlayer;
import battlecode.common.*;
/**
 * Mostly number functions (get random numbers, etc)
 * Created by raytr on 1/20/2017.
 */
public class Util {
    public static Direction getRandomDir(){ return new Direction((float)Math.random() * 2 * (float)Math.PI); }

    public static int getRandomIntInclusive(int min,int max){
        return min + (int)(Math.random() * ((max - min) + 1));
    }

    public static int getNthDigit(int number, int base, int n) {
        return (int) ((number / Math.pow(base, n - 1)) % base);
    }

    public static int getNumberDigits(Integer number){
        return number.toString().length();
    }
}

