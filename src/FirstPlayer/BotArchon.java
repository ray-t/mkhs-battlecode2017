package FirstPlayer;
import battlecode.common.*;
/**
 * Created by raytr on 1/20/2017.
 */
public class BotArchon extends BotBase {
    public static void loop() throws GameActionException {
        while(true){
            checkPay2Win();
            shakeNearbyTrees();

            try {
                if (BroadcastHandler.canClearBroadcastYet()){
                    if (!BroadcastHandler.getClearedToBroadcast()) {
                        BroadcastHandler.clearAllBroadcasts();
                        BroadcastHandler.setClearedToBroadcast(true);
                    }
                }
                if (BroadcastHandler.canSendBroadcastYet()){

                }

                if (BroadcastHandler.canReceiveBroadcastYet()){
                    BroadcastHandler.setClearedToBroadcast(false);
                    //do something lol
                }



                //Move randomly
                Direction randDir = Util.getRandomDir();
                tryMove(randDir);

                // Randomly attempt to build a gardener in this direction
                if (rc.canHireGardener(randDir)) {
                    rc.hireGardener(randDir);
                }




                Clock.yield();

            } catch (Exception e) {
                System.out.println("Archon Exception");
                e.printStackTrace();
            }
        }
    }
}
